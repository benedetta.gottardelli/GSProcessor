# -------------------------------------------------------
#  Import all your packages
# -------------------------------------------------------

library(jsonlite)
library(data.table)
# etc.

# -------------------------------------------------------




# -------------------------------------------------------
#  FIXED - Read parameters passed by the Processor


paramfileName <- commandArgs(trailingOnly = TRUE)[1]

# -------------------------------------------------------

main <- function(paramfileName) {
  # -------------------------------------------------------
  # FIXED - Read all exectution parameters

  paramData <- fromJSON(file = paramfileName)

  # parameters
  run_id <- paramData$header$run.id
  tokenInstanceUID <- paramData$header$tokenInstanceUID
  dataSourceAlias <- paramData$payload$lst.dataSource
  inputCSVFile <- lst.parametri$payload$lst.dataSource[[ lst.parametri$payload$lst.run[[run.id]]$dataSourceAlias ]]$dataSourceFileName
  outputFolder <- paramData$header$executionFolderFullName
  token_waiting_filename <- paramData$header$token.waiting.filename
  currentDir <- paramData$header$currentDir


  # Read data
  DB <- read.csv(paste(c(currentDir,"/",inputCSVFile),collapse = ''))


  # -------------------------------------------------------

  # Run your algorithm

  print(run_id, tokenInstanceUID, dataSourceAlias, inputCSVFile, outputFolder, token_waiting_filename)

  # Create a list of result files and their types
  result_dict <- list(fileName1 = "fileType1", fileName2 = "fileType2") # Add your actual result files and types

  # -------------------------------------------------------

   # -------------------------------------------------------
   # FIXED - Generate the output XML (header section + 1 object section per result file)
   # -------------------------------------------------------

  header_xml_string <- sprintf(
    "<XMLheader>
      <tokenInstanceUID>%s</tokenInstanceUID>
      <runUID>%s</runUID>
      <creationDateTime>%s</creationDateTime>
    </XMLheader>",
    tokenInstanceUID, run_id, format(Sys.time(), "%Y/%m/%d %H:%M:%S")
  )

  obj_xml_strings <- lapply(names(result_dict), function(file) {
    sprintf(
      "<XMLobj objType='other'>
        <resourceFileType>%s</resourceFileType>
        <resourceFileName>%s</resourceFileName>
      </XMLobj>",
      result_dict[file], file
    )
  })

  xml_output_string <- paste("<xml>", header_xml_string, obj_xml_strings, "<xml>", sep = "\n")

  # Save the XML descriptor file for the output token
  writeLines(xml_output_string, file.path(outputFolder, "description.xml"))
  cat("done\n")

  # Notify the processor that the execution is done
  writeLines("done", token_waiting_filename)
}

main(paramfileName)

