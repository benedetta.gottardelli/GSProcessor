# Run R scripts

GSProcessor pacckage is comaptible with R scripts. Here the instructions to create token incapsulating a R script. 

## Requirements

- Linux OS (Ubuntu >=20.04; Debian >= 11)
- R (v4.0.4)
- Install in advance on your host machine all the packages you need for running your script


## Create token

A token for launching an algorithm in R script MUST have this minimal structure:

		.
		├── csvData.csv
		├── description.xml
		└── script_R.r

Any other accessory file can be included in the zip file as well. A template can be found in the "token_template".

#### NOTE THAT: 
- In the "script_R.r" you can find a template for structuring your algorithm code. Some sections are mandatory and ment to be copied and pasted in your new R script file.

#### Fill the descrition.xml
Fill the *header* with: 
- **tokenInstanceUID**:  your ID for the task
- **runUID**: your ID for the task run
- **creationDateTime**: datetime when you created the token


		<XMLheader>
			 <tokenInstanceUID>##tokenInstanceUID##</tokenInstanceUID>  
			 <runUID>##runUID##</runUID>  
			 <creationDateTime>##creationDateTime##</creationDateTime> 
		</XMLheader>

 
Fill the *Data scetion* with: 
- **dataSourceType**:  data source type
- **dataSourceFileName**: data filename
- **alias**: arbitrary alias for data

		# Data 
		<XMLobj  objType='dataSource'>
			 <dataSourceType>csv</dataSourceType>  
			 <dataSourceFileName>##dataSourceFileName##</dataSourceFileName>  
			 <alias>##dataAlias##</alias>
		</XMLobj>
		
Fill the *Script scetion* with: 
-  **alias**: arbitrary alias for the algorithm
- **scriptType**:  set it to "script"
- **scriptProgrammingLanguage**: set it to "R"
- **scriptFileName**: script filename
-  **processorConformanceClass**: set it to "R"

		# Script
		<XMLobj objType='script'>
	    <alias>1</alias>
	    <scriptType>script</scriptType>
	    <scriptProgrammingLanguage>R</scriptProgrammingLanguage>
	    <scriptFileName>##scriptFilename##</scriptFileName>
	    <processorConformanceClass>R</processorConformanceClass>
	  </XMLobj>

Add one *Other section* per additional file you attach in the token zip file (other then datafile and description.xml) and fill it with: 
- **filename**: filename of the additional file
- **alias**: arbitrary alias for the additional file

		# Other (Optional) - Add one section per additional file you attach in the token zip file
		<XMLobj  objType='other'>
			 <alias>##otherAlias##</alias> 
			 <filename>##otherFilename##</filename> 
		</XMLobj>

Fill the *Run scetion* with: 
- **alias**:  paste the runUID from the header
- **scriptAlias**: paste the scriptAlias from the 'script' section
- **otherAlias**: paste the otherAlias from the 'other' section (if multiple additional files, add multiple tags)
- **dataSourceAlias**: paste the dataSourceAlias from the 'data' section

		# Run
		<XMLobj  objType='run'>
			 <alias>##runAlias##</alias>  
			 <scriptAlias>##scriptAlias##</scriptAlias> 
			 <otherAlias>###otherAlias###</otherAlias>
			 <dataSourceAlias>##scriptAlias##</dataSourceAlias>
			 <outXMLDescriptorType>attached</outXMLDescriptorType> # don't change
		</XMLobj>
	</xml>

Finally, zip the token folder to create a token zip file.

