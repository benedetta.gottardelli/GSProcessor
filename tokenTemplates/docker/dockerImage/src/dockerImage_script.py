# -------------------------------------------------------
#  Import all your packages
# -------------------------------------------------------

import numpy as np
import os
# etc.



# -------------------------------------------------------
#  FIXED - Read parameters passed by the Processor 
# -------------------------------------------------------

paramfileName = sys.argv[2]
path = sys.argv[1]



# -------------------------------------------------------
#  Read parameters passed by the Processor
# -------------------------------------------------------

def main(path, paramfileName):

    # -------------------------------------------------------
    # FIXED - go to working dir
    os.chdir(path)
    # -------------------------------------------------------
    
    
    # -------------------------------------------------------
    # FIXED - Read all exectution parameters
    with open(paramfileName, 'r') as f:  # parameters are passed to the docker images in a json format from the Processor
        lst_parametri = json.load(f)

    # parameters
    run_id =  lst_parametri['header']['run.id'][0] # run ID
    tokenInstanceUID = lst_parametri['header']['tokenInstanceUID'][0] # token ID 
    dataSourceAlias = list(lst_parametri['payload']['lst.dataSource'].keys())[0]  # data file name
    outputFolder = lst_parametri['header']['executionFolderFullName'][0]  # location when all the results MUST be samed 
    
    # FREE - read any extra in the paramFile (accessory files attached in the token zip)
    # e.g :  attachehFile = lst_parametri['payload']['lst.other']['columnList']
    # -------------------------------------------------------


    # -------------------------------------------------------
    # Run your function
    
    result_dict = yourFunction(lst_parametri, dataSourceAlias, outputFolder,  ...)
    
    # -------------------------------------------------------
    
    
    # -------------------------------------------------------
    # FIXED - Generate the output XML (header section + 1 object section per result file)
    # -------------------------------------------------------
    
    xml_output_string = "<xml>"
    
    header_xml_string = """<XMLheader>
                                                  <tokenInstanceUID>##tokenInstanceUID##</tokenInstanceUID>
                                                  <runUID>##runUID##</runUID>
                                                  <creationDateTime>##creationDateTime##</creationDateTime>
                                        </XMLheader>
                                    """
    filled_header_xml_string = header_xml_string.replace("##tokenInstanceUID##",tokenInstanceUID)
    filled_header_xml_string = filled_header_xml_string.replace("##runUID##", run_id)
    filled_header_xml_string = filled_header_xml_string.replace("##creationDateTime##",datetime.now().strftime("%Y/%m/%d %H:%M:%S"))
    
   xml_output_string += filled_header_xml_string
   
    # fill one obj_xml_string per result file
    obj_xml_string = """<XMLobj objType='other'>
                                                  <resourceFileType>##resourceFileType##</resourceFileType>
                                                  <resourceFileName>##resourceFileName##</resourceFileName>
                                  </XMLobj>"""
    
    
    for file in result_dict: 
        filled_obj_xml_string = obj_xml_string.replace("##resourceFileName##", file)
        filled_obj_xml_string = filled_obj_xml_string.replace("##resourceFileType##",result_dict[file])
        xml_output_string += filled_obj_xml_string
        
    xml_output_string += "<xml>"
      
    ## Save the xml descriptor file for the output token 
                          
    text_file = open(outputFolder + "/description.xml", "w")
    n = text_file.write(xml_output_string)
    text_file.close()
    print("done")



def yourFunction(lst_parametri, dataSourceAlias, outputFolder,  ...):
    # saves all the execution results in outputFolder and returns a dictionary of all result files and types e.g. : {'plot1.html': 'Survival Plots', 'plot2.html': 'Box Plot', ...}
    return {'fileName1': 'fileType1',  'fileName2': 'fileType2' ... } 
        

if __name__ == "__main__":
    main(path, paramfileName)
