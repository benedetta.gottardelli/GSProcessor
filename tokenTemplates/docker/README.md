# Run tasks with Docker

GSProcessor pacckage is comaptible with Docker and able to run pre-built docker images.

## Requirements

- Linux OS (Ubuntu >=20.04; Debian >= 11)
- Docker (installation instruction from the official web site https://docs.docker.com/engine/install)

## Create your Docker image compatible with GSProcessor

Create a Docker image folder with the following recommended structure:

	.
	├── Dockerfile							
	├── requirements.txt
	└── src
	    ├── dockerImage_script.py  # script that launches the algorithm
	    └── dockerImage.sh         # bash script for starting the execution and notify when it is done

Check the template in "dockerImage" folder.

#### NOTE THAT: 
- It is important to have a script such as "dockerImage.sh" that launches the exectution and then notifies the Processor when it is over by creating a file in the Processor's sync folder.
- In the "dockerImage_script.py" you can find a template for structuring your algorithm code. Some sections are mandatory and ment to be copied or tanslated in case of an other programming language.
- In the Dockerfile the command to run on container start must be an ENTRYPOINT launching  "dockerImage.sh" : 
		

			    # command to run on container start
    			ENTRYPOINT [ "sh","dockerImage.sh"]

To build your Docker image run: 

	docker build -t <your_docker_image_name> . 
	
<your_docker_image_name> is going to be the name you will refer to in the token descriptor file.

## Create token

A token for launching an algorithm encapsulated in a Docker image MUST have this minimal structure:

		.
		├── data.csv
		└── description.xml

A data file and a descrition.xml are required. Any other accessory file can be included in the zip file as well. 

#### Fill the descrition.xml
Fill the *header* with: 
- **tokenInstanceUID**:  your ID for the task
- **runUID**: your ID for the task run
- **creationDateTime**: datetime when you created the token


		<XMLheader>
			 <tokenInstanceUID>##tokenInstanceUID##</tokenInstanceUID>  
			 <runUID>##runUID##</runUID>  
			 <creationDateTime>##creationDateTime##</creationDateTime> 
		</XMLheader>

 
Fill the *Data scetion* with: 
- **dataSourceType**:  data source type
- **dataSourceFileName**: data filename
- **alias**: arbitrary alias for data

		# Data 
		<XMLobj  objType='dataSource'>
			 <dataSourceType>csv</dataSourceType>  
			 <dataSourceFileName>##dataSourceFileName##</dataSourceFileName>  
			 <alias>##dataAlias##</alias>
		</XMLobj>
		
Fill the *Data scetion* with: 
- **scriptType**:  set it to "docker"
- **dockerImageName**: <your_docker_image_name> 
- **alias**: arbitrary alias for the algorithm

		# Script
		<XMLobj  objType='script'>
			 <alias>##scriptAlias##</alias> # arbitrary alias for the algorithm
			 <scriptType>docker</scriptType>
			 <dockerImageName>##dockerImageName##</dockerImageName> 
		</XMLobj>

Add one *Other section* per additional file you attach in the token zip file (other then datafile and description.xml) and fill it with: 
- **filename**: filename of the additional file
- **alias**: arbitrary alias for the additional file

		# Other (Optional) - Add one section per additional file you attach in the token zip file
		<XMLobj  objType='other'>
			 <alias>##otherAlias##</alias> 
			 <filename>##otherFilename##</filename> 
		</XMLobj>

Fill the *Run scetion* with: 
- **alias**:  paste the runUID from the header
- **scriptAlias**: paste the scriptAlias from the 'script' section
- **otherAlias**: paste the otherAlias from the 'other' section (if multiple additional files, add multiple tags)
- **dataSourceAlias**: paste the dataSourceAlias from the 'data' section

		# Run
		<XMLobj  objType='run'>
			 <alias>##runAlias##</alias>  
			 <scriptAlias>##scriptAlias##</scriptAlias> 
			 <otherAlias>###otherAlias###</otherAlias>
			 <dataSourceAlias>##scriptAlias##</dataSourceAlias>
			 <outXMLDescriptorType>attached</outXMLDescriptorType> # don't change
		</XMLobj>
	</xml>

Finally, zip the token folder to create a token zip file. 
A token template can be found the "token_templates" folder.

