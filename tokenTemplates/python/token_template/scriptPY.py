# -------------------------------------------------------
#  Import all your packages
# -------------------------------------------------------

import numpy as np
import os
# etc.

# -------------------------------------------------------
#  FIXED - Read parameters passed by the Processor 
# -------------------------------------------------------

paramfileName = sys.argv[1]

def main(paramfileName):

    # -------------------------------------------------------
    # FIXED - Read all exectution parameters
    
    dirname = os.path.dirname(paramfileName)
    cwd = os.getcwd()
    with open(paramfileName, 'r') as f:
        lst_parametri = json.load(f)
        
    # parameters
    run_id =  lst_parametri['header']['run.id'][0] # run ID
    tokenInstanceUID = lst_parametri['header']['tokenInstanceUID'][0] # token ID 
    dataSourceAlias = list(lst_parametri['payload']['lst.dataSource'].keys())[0]  # data file name
    outputFolder = lst_parametri['header']['executionFolderFullName'][0]  # location when all the results MUST be samed 
    token_waiting_filename = lst_parametri["header"]["token.waiting.filename"][0]
    
    # FREE - read any extra in the paramFile (accessory files attached in the token zip)
    # e.g :  attachehFile = lst_parametri['payload']['lst.other']['columnList']
    # -------------------------------------------------------

    # -------------------------------------------------------
    # FIXED - Read data and change working dir
    
    DB = pd.read_csv(lst_parametri['payload']['lst.dataSource'][run_id]['dataSourceFileName'][0])
    os.chdir(cwd)
    # -------------------------------------------------------
    
    
    # -------------------------------------------------------
    # Run your algorithm
    
    ...
    
    
    result_dict    = {'fileName1': 'fileType1',  'fileName2': 'fileType2' ... }   #dictionary of all result files and types e.g. : {'plot1.html': 'Survival Plots', 'plot2.html': 'Box Plot', ...}
    # -------------------------------------------------------
    
    # -------------------------------------------------------
    # FIXED - Generate the output XML (header section + 1 object section per result file)
    # -------------------------------------------------------
    
    xml_output_string = "<xml>"
    
    header_xml_string = """<XMLheader>
                                                  <tokenInstanceUID>##tokenInstanceUID##</tokenInstanceUID>
                                                  <runUID>##runUID##</runUID>
                                                  <creationDateTime>##creationDateTime##</creationDateTime>
                                        </XMLheader>
                                    """
    filled_header_xml_string = header_xml_string.replace("##tokenInstanceUID##",tokenInstanceUID)
    filled_header_xml_string = filled_header_xml_string.replace("##runUID##", run_id)
    filled_header_xml_string = filled_header_xml_string.replace("##creationDateTime##",datetime.now().strftime("%Y/%m/%d %H:%M:%S"))
    
   xml_output_string += filled_header_xml_string
   
    # fill one obj_xml_string per result file
    obj_xml_string = """<XMLobj objType='other'>
                                                  <resourceFileType>##resourceFileType##</resourceFileType>
                                                  <resourceFileName>##resourceFileName##</resourceFileName>
                                  </XMLobj>"""
    
    
    for file in result_dict: 
        filled_obj_xml_string = obj_xml_string.replace("##resourceFileName##", file)
        filled_obj_xml_string = filled_obj_xml_string.replace("##resourceFileType##",result_dict[file])
        xml_output_string += filled_obj_xml_string
        
    xml_output_string += "<xml>"
      
    ## Save the xml descriptor file for the output token 
                          
    text_file = open(outputFolder + "/description.xml", "w")
    n = text_file.write(xml_output_string)
    text_file.close()
    print("done")
    
    ## Notify the processor execution is done 
    text_file = open(token_waiting_filename, "w")
    n = text_file.write("done")
    text_file.close()



        
if __name__ == "__main__":
    main(paramfileName)
