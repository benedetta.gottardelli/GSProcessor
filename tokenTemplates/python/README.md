# Run Python scripts

GSProcessor pacckage is comaptible with Python scripts. Here the instructions to create token incapsulating a Python script. 

## Requirements

- Linux OS (Ubuntu >=20.04; Debian >= 11)
- Python (v3.)
- "venv" for Python 3 (https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/)


## Create token

A token for launching an algorithm in python script MUST have this minimal structure:

		.
		├── csvData.csv
		├── description.xml
		├── requirements.txt (optional but highly recommended)
		└── scriptPY.py

Any other accessory file can be included in the zip file as well. A template can be found in the "token_template".

#### NOTE THAT: 
- In the "scriptPY.py" you can find a template for structuring your algorithm code. Some sections are mandatory and ment to be copied and pasted in your new python script file.

#### Fill the descrition.xml
Fill the *header* with: 
- **tokenInstanceUID**:  your ID for the task
- **runUID**: your ID for the task run
- **creationDateTime**: datetime when you created the token


		<XMLheader>
			 <tokenInstanceUID>##tokenInstanceUID##</tokenInstanceUID>  
			 <runUID>##runUID##</runUID>  
			 <creationDateTime>##creationDateTime##</creationDateTime> 
		</XMLheader>

 
Fill the *Data scetion* with: 
- **dataSourceType**:  data source type
- **dataSourceFileName**: data filename
- **alias**: arbitrary alias for data

		# Data 
		<XMLobj  objType='dataSource'>
			 <dataSourceType>csv</dataSourceType>  
			 <dataSourceFileName>##dataSourceFileName##</dataSourceFileName>  
			 <alias>##dataAlias##</alias>
		</XMLobj>
		
Fill the *Script scetion* with: 
-  **alias**: arbitrary alias for the algorithm
- **scriptType**:  set it to "script"
- **scriptProgrammingLanguage**: set it to "Python"
- **scriptFileName**: script filename
-  **processorConformanceClass**: set it to "Python"

		# Script
		<XMLobj objType='script'>
	    <alias>1</alias>
	    <scriptType>script</scriptType>
	    <scriptProgrammingLanguage>Python</scriptProgrammingLanguage>
	    <scriptFileName>##scriptFilename##</scriptFileName>
	    <processorConformanceClass>Python</processorConformanceClass>
	  </XMLobj>

Add one *Other section* per additional file you attach in the token zip file (other then datafile and description.xml) and fill it with: 
- **filename**: filename of the additional file
- **alias**: arbitrary alias for the additional file

		# Other (Optional) - Add one section per additional file you attach in the token zip file
		<XMLobj  objType='other'>
			 <alias>##otherAlias##</alias> 
			 <filename>##otherFilename##</filename> 
		</XMLobj>

Fill the *Run scetion* with: 
- **alias**:  paste the runUID from the header
- **scriptAlias**: paste the scriptAlias from the 'script' section
- **otherAlias**: paste the otherAlias from the 'other' section (if multiple additional files, add multiple tags)
- **dataSourceAlias**: paste the dataSourceAlias from the 'data' section

		# Run
		<XMLobj  objType='run'>
			 <alias>##runAlias##</alias>  
			 <scriptAlias>##scriptAlias##</scriptAlias> 
			 <otherAlias>###otherAlias###</otherAlias>
			 <dataSourceAlias>##scriptAlias##</dataSourceAlias>
			 <outXMLDescriptorType>attached</outXMLDescriptorType> # don't change
		</XMLobj>
	</xml>

Finally, zip the token folder to create a token zip file.

