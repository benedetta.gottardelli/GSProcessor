# Install GSProcessor package

devtools::install_gitlab("benedetta.gottardelli/GSProcessor")

# Import packages
library(GSProcessor)
library(jsonlite)
library(XML)
library(stringr)
library(zip)


# Instanciate processor (note: create in advance all the necessary folder e.g. input-, output-, sync- and tmpFolder)
objS <- bckGndProcessor(input_folder.dir = "./folders/inputFolder/",
                        output_folder.dir = "./folders/outputFolder/",
                        tmp_folder.dir = "./folders/tmpFolder/",
                        sync_folder.dir = "./folders/syncFolder/",
                        override.repeated.tUID = TRUE
)

objS$start()
